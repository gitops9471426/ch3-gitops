# ch3-gitops

## Install
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm


## Front
https://front.ahmed-benmessaoud.takima.school

## Helm

$ helm template --values ./values.yaml ./ --output-dir dist
$ helm install cdb ./
$ helm upgrade cdb ./